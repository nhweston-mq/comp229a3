package au.edu.mq.students.s44882017.comp229.boardgame;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;

public class GfxHandler {

    public static String GFX_DIR;

    private static Map<Class, Map<Integer, Image>> gfx = new Hashtable<>();

    public static void set(String typeName, String filePath) {
        set(typeName, filePath, 0);
    }

    public static void set(String typeName, String filePath, int variant) {
        try {
            Class type = Class.forName(typeName);
            set(type, filePath, variant);
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void set(Class type, String filePath, int variant) {
        gfx.putIfAbsent(type, new Hashtable<>());
        try {
            gfx.get(type).put(variant, ImageIO.read(new File(filePath)));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Image get(Class type) {
        return get(type, 0);
    }

    public static Image get(Class type, int variant) {
        return gfx.getOrDefault(type, new Hashtable<>()).get(variant);
    }

}

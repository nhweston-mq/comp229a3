/*
 * COMP229 A3
 * Nicholas Weston (44882017)
 */

package au.edu.mq.students.s44882017.comp229.boardgame;

import au.edu.mq.students.s44882017.comp229.boardgame.celltypes.CellTypeGenerator;

import javax.swing.*;
import java.awt.*;
import java.util.*;

public class Board<T extends Enum & CellType> extends JPanel {

    private static Random random = new Random();

    private static final Color COL_STROKE_DEF, COL_BG_DEF, COL_HIGHLIGHT_DEF;

    static {
        COL_STROKE_DEF = new Color(255, 255, 255);
        COL_BG_DEF = new Color(0, 0, 0);
        COL_HIGHLIGHT_DEF = new Color(255, 255, 255, 128);
    }

    private int posX, posY;
    private int cellSize;
    private Color colStroke;
    private Cell[][] cells;
    private Set<Piece> pieces;
    private State state;
    private long turnLength;

    public Board(int posX, int posY, int dimX, int dimY, int cellSize) {
        setPos(posX, posY);
        initCells(dimX, dimY, cellSize);
        this.colStroke = COL_STROKE_DEF;
        pieces = new HashSet<>();
    }

    public void generateTerrain(CellTypeGenerator<T> cellTypeGenerator) {
        for (int x=0; x<cells.length; x++) for (int y=0; y<cells[0].length; y++) {
            cells[x][y].setCellType(cellTypeGenerator.get(x, y));
        }
    }

    public void addPiece(Piece piece) {
        pieces.add(piece);
    }

    public void nextTurn(int turnLength) {
        this.turnLength = turnLength;
    }

    public void paint(Graphics g) {
        for (int x=0; x<cells.length; x++) for (int y=0; y<cells[0].length; y++) {
            cells[x][y].paint(g, posX+cellSize*x, posY+cellSize*y, cellSize, colStroke);
        }
    }

    public boolean isCellValid(int x, int y) {
        return (x >= 0 && x < cells.length && y >= 0 && y < cells[0].length);
    }

    public Cell getCell(int x, int y) {
        if (isCellValid(x, y)) {
            return cells[x][y];
        }
        return null;
    }

    public Cell getAdjacentCell(Cell cell, Direction direction) {
        return getAdjacentCell(cell.index.x, cell.index.y, direction);
    }

    public Cell getAdjacentCell(int x, int y, Direction direction) {
        switch (direction) {
            case UP:    y--;    break;
            case DOWN:  y++;    break;
            case LEFT:  x--;    break;
            case RIGHT: x++;    break;
        }
        return getCell(x, y);
    }

    public Cell getRelativeCell(Cell cell, int dX, int dY) {
        return getRelativeCell(cell.index.x, cell.index.y, dX, dY);
    }

    public Cell getRelativeCell(int x, int y, int dX, int dY) {
        return getCell(x+dX, y+dY);
    }

    public Cell getRandomCell() {
        return cells[random.nextInt(cells.length)][random.nextInt(cells[0].length)];
    }

    private void setPos(int posX, int posY) {
        this.posX = posX;
        this.posY = posY;
    }

    private void initCells(int dimX, int dimY, int cellSize) {
        this.cellSize = cellSize;
        cells = new Cell[dimX][dimY];
        for (int x=0; x<dimX; x++) for (int y=0; y<dimY; y++) {
            cells[x][y] = new Cell(this, x, y);
        }
    }

    public Piece findPieceByType(Class type) {
        for (Piece piece : pieces) {
            if (piece.getClass().equals(type)) {
                return piece;
            }
        }
        return null;
    }

    public Collection<Piece> pieceSet() {
        return new HashSet<>(pieces);
    }

}

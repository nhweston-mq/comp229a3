package au.edu.mq.students.s44882017.comp229.boardgame;

public enum Direction {

    UP,
    DOWN,
    LEFT,
    RIGHT;

}

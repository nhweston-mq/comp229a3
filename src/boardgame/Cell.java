package au.edu.mq.students.s44882017.comp229.boardgame;

import java.awt.*;
import java.util.*;
import java.util.List;

public class Cell {

    private static PieceDepthComparator pieceDepthComparator = new PieceDepthComparator();

    final Point index;
    private Board board;
    private CellType cellType;
    private List<Piece> pieces;

    Cell(Board board, int x, int y) {
        this.board = board;
        index = new Point(x, y);
    }

    synchronized void addPiece(Piece piece) {
        if (pieces == null) {
            pieces = new LinkedList<>();
        }
        pieces.add(piece);
        for (Piece pieceOther : pieces) {
            piece.cellCohabited(pieceOther);
            pieceOther.cellCohabited(piece);
        }
    }

    synchronized void removePiece(Piece piece) {
        if (pieces == null) {
            return;
        }
        pieces.remove(piece);
        if (pieces.isEmpty()) {
            pieces = null;
        }
    }

    void paint(Graphics g, int posX, int posY, int cellSize, Color colStroke) {
        g.setColor(cellType.getCol());
        g.fillRect(posX, posY, cellSize, cellSize);
        g.setColor(colStroke);
        g.drawRect(posX, posY, cellSize, cellSize);
        if (pieces != null) for (Piece piece : pieces) {
            piece.paint(g, posX, posY);
        }
    }

    void setCellType(CellType cellType) {
        this.cellType = cellType;
    }

    public int getX() {
        return index.x;
    }

    public int getY() {
        return index.y;
    }

    public int getDistX(Cell other) {
        return other.index.x - this.index.x;
    }

    public int getDistY(Cell other) {
        return other.index.y - this.index.y;
    }

    public Cell getRelativeCell(int dX, int dY) {
        return board.getRelativeCell(index.x, index.y , dX, dY);
    }

    public boolean isAdjacentTo(Cell other) {
        int dX = Math.abs(this.index.x - other.index.x);
        int dY = Math.abs(this.index.y - other.index.y);
        return (dX == 0 && dY == 1) || (dX == 1 && dY == 0);
    }

    public Board getBoard() {
        return board;
    }

    public Collection<Piece> getPieces() {
        return new HashSet<>(pieces);
    }

}
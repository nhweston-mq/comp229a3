package au.edu.mq.students.s44882017.comp229.boardgame;

import java.time.Instant;
import java.util.LinkedList;
import java.util.List;

public class Turn extends Thread {

    Board board;
    Instant endTime;

    Turn(Board board, long length) {
        this.board = board;
        endTime = Instant.now().plusMillis(length);
    }

    @Override
    public void run() {
        while (Instant.now().isBefore(endTime)) {

        }
    }

}

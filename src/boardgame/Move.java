package au.edu.mq.students.s44882017.comp229.boardgame;

import java.awt.*;

public class Move {

    Piece piece;
    private Cell cellFrom, cellTo;

    public Move(Piece piece, Cell cellFrom, Cell cellTo) {
        this.piece = piece;
        this.cellFrom = cellFrom;
        this.cellTo = cellTo;
    }

    public synchronized boolean perform() {
        return piece.move(cellFrom, cellTo);
    }

}

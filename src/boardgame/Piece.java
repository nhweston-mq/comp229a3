package au.edu.mq.students.s44882017.comp229.boardgame;

import java.awt.*;

public abstract class Piece {

    private Cell cell;
    private boolean active;
    private int depth;

    public Piece(Board board) {
        setBoard(board);
        active = true;
    }

    public abstract boolean isValidMove(Cell cellTo);

    public abstract void paint(Graphics g, int posX, int posY);

    public abstract void cellCohabited(Piece other);

    public void setBoard(Board board) {
        setCell(board.getRandomCell());
        board.addPiece(this);
    }

    public Board getBoard() {
        return cell.getBoard();
    }

    public final boolean isMoveValid(Cell cellFrom, Cell cellTo) {
        return (
            cellFrom != null &&
            cellTo != null &&
            cellFrom.equals(cell) &&
            isValidMove(cellTo)
        );
    }

    void setCell(Cell cell) {
        if (cell != null) {
            cell.addPiece(this);
        }
        if (this.cell != null) {
            this.cell.removePiece(this);
        }
        this.cell = cell;
    }

    public boolean isActive() {
        return active;
    }

    public void remove() {
        setCell(null);
        this.active = false;
    }

    boolean move(Cell cellFrom, Cell cellTo) {
        if (!isMoveValid(cellFrom, cellTo)) {
            return false;
        }
        else {
            setCell(cellTo);
        }
        moveNotify(cellFrom, cellTo);
        return true;
    }

    public void moveNotify(Cell cellFrom, Cell cellTo) {}

    public Cell getCell() {
        return cell;
    }

    int getDepth() {
        return depth;
    }

}
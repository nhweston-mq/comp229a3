package au.edu.mq.students.s44882017.comp229.boardgame;

import java.awt.*;

public interface CellType {

    Color getCol();
    Image getImage();

}
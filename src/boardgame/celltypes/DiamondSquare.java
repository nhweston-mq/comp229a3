package au.edu.mq.students.s44882017.comp229.boardgame.celltypes;

import java.awt.*;
import java.util.Hashtable;
import java.util.Map;
import java.util.Random;

public class DiamondSquare {

    private int dimX, dimY;
    private double incrLimitRatio;
    private Random random;
    private Map<Point, Double> fractal;

    public DiamondSquare(int dimX, int dimY, double incrLimitRatio) {
        this.dimX = dimX;
        this.dimY = dimY;
        this.incrLimitRatio = incrLimitRatio;
        random = new Random();
        generate();
    }

    public Map<Point, Double> get() {
        return fractal;
    }

    private void generate() {
        // Find the smallest integer of form n^2+1 larger than both dimX and dimY.
        double minPowOfTwo = Math.pow(2.0, Math.ceil(Math.max(Math.log(dimX-1), Math.log(dimY-1)) / Math.log(2)));
        int dim = (new Double(minPowOfTwo)).intValue() + 1;
        double[][] result = new double[dim][dim];
        double incrLimit = 1.0;
        // Initialise corners.
        for (int i=0; i<4; i++) {
            result[(i%2)*(dim-1)][(i/2)*(dim-1)] = genFracIncr(incrLimit);
        }
        // Diamond-square.
        for (int scale=dim-1; scale>1; scale=scale/2) {
            incrLimit = incrLimit * incrLimitRatio;
            fracDiamond(result, scale, incrLimit);
            fracSquare(result, scale, incrLimit);
        }
        // Write to Map.
        fractal = new Hashtable<>();
        for (int x=0; x<dimX; x++) for (int y=0; y<dimY; y++) {
            fractal.put(new Point(x, y), result[x][y]);
        }
    }

    private void fracDiamond(double[][] arr, int scale, double incrLimit) {
        for (int x=scale/2; x<arr.length; x=x+scale) {
            for (int y=scale/2; y<arr[0].length; y=y+scale) {
                double sum = 0;
                int nPoints = 0;
                for (int iCorner=0; iCorner<4; iCorner++) {
                    // Center is sum of corners
                    int xCorner = x-scale/2+(iCorner%2)*2*(scale/2);
                    int yCorner = y-scale/2+(iCorner/2)*2*(scale/2);
                    if (xCorner >= 0 && xCorner < arr.length && yCorner >= 0 && yCorner < arr[0].length) {
                        sum = sum + arr[xCorner][yCorner];
                        nPoints++;
                    }
                }
                arr[x][y] = sum/nPoints + genFracIncr(incrLimit);
            }
        }
    }

    private void fracSquare(double[][] arr, int scale, double incrLimit) {
        for (int x=0; x<arr.length; x=x+scale/2) {
            int startY = 0;
            if (x % scale == 0) {
                startY = scale/2;
            }
            for (int y=startY; y<arr[0].length; y=y+scale) {
                double sum = 0;
                int nPoints = 0;
                for (int iAdj=0; iAdj<4; iAdj++) {
                    // Center is sum of adjacencies
                    int xAdj = x + (scale/2) * ((iAdj+1)%2) * (iAdj-1);
                    int yAdj = y + (scale/2) * (iAdj%2) * (iAdj-2);
                    if (xAdj >= 0 && xAdj < arr.length && yAdj >= 0 && yAdj < arr[0].length) {
                        sum = sum + arr[xAdj][yAdj];
                        nPoints++;
                    }
                }
                arr[x][y] = sum/nPoints + genFracIncr(incrLimit);
            }
        }
    }

    private double genFracIncr(double incrLimit) {
        return incrLimit * (2*random.nextDouble() - 1);
    }

}

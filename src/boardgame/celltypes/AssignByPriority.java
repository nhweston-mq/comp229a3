package au.edu.mq.students.s44882017.comp229.boardgame.celltypes;

import au.edu.mq.students.s44882017.comp229.boardgame.Cell;
import au.edu.mq.students.s44882017.comp229.boardgame.CellType;

import java.awt.*;
import java.util.*;

public class AssignByPriority<T extends Enum & CellType> implements CellTypeGenerator<T> {

    private int dimX, dimY;
    private T typeDefault;
    private Map<Point, T> cellTypes;
    private Set<Point> unassigned;
    private Map<T, Map<Point, Double>> scoreTables;
    private Map<T, Integer> numsToAssign;

    private class ScoreNode {

        Point cell;
        T terrain;

        private ScoreNode(Point cell, T terrain) {
            this.cell = cell;
            this.terrain = terrain;
        }

    }

    private class ScoreComparator implements Comparator<ScoreNode> {

        public int compare(ScoreNode i1, ScoreNode i2) {
            double score1 = getScore(i1.cell, i1.terrain);
            double score2 = getScore(i2.cell, i2.terrain);
            return Double.compare(score1, score2);
        }

    }

    public AssignByPriority(int dimX, int dimY, T typeDefault) {
        this.typeDefault = typeDefault;
        this.dimX = dimX;
        this.dimY = dimY;
        unassigned = new HashSet<>();
        for (int x=0; x<dimX; x++) for (int y=0; y<dimY; y++) {
            unassigned.add(new Point(x, y));
        }
        cellTypes = new Hashtable<>();
        scoreTables = new Hashtable<>();
        numsToAssign = new Hashtable<>();
    }

    public void assign(Collection<T> typesToAssign) {
        PriorityQueue<ScoreNode> pq = new PriorityQueue<>(new ScoreComparator());
        for (T cellType : typesToAssign) {
            for (Point cell : unassigned) {
                pq.add(new ScoreNode(cell, cellType));
            }
        }
        int numTypesToAssign = typesToAssign.size();
        int numTypesFinished = 0;
        while (numTypesFinished < numTypesToAssign && !pq.isEmpty()) {
            ScoreNode node = pq.poll();
            int numToAssign = numsToAssign.getOrDefault(node.terrain, 0);
            if (numToAssign > 0) {
                cellTypes.put(node.cell, node.terrain);
                unassigned.remove(node.cell);
                numToAssign--;
                numsToAssign.put(node.terrain, numToAssign);
                if (numToAssign == 0) {
                    numTypesFinished++;
                }
            }
        }
    }

    public void setScoreTable(T cellType, Map<Point, Double> table) {
        scoreTables.put(cellType, table);
    }

    public void setNumToAssign(T cellType, int numToAssign) {
        numsToAssign.put(cellType, numToAssign);
    }

    @Override
    public T get(int x, int y) {
        if (cellTypes == null) {
            return typeDefault;
        }
        T result = cellTypes.get(new Point(x, y));
        if (result == null) {
            return typeDefault;
        }
        return result;
    }

    protected int getDimX() {
        return dimX;
    }

    protected int getDimY() {
        return dimY;
    }

    private double getScore(Point cell, T cellType) {
        Map<Point, Double> table = scoreTables.get(cellType);
        if (table == null) {
            return 0;
        }
        return table.getOrDefault(cell, 0.0);
    }

}

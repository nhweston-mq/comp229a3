package au.edu.mq.students.s44882017.comp229.boardgame.celltypes;

import au.edu.mq.students.s44882017.comp229.boardgame.CellType;

import java.awt.*;

public interface CellTypeGenerator<T extends Enum & CellType> {

    T get(int x, int y);

}
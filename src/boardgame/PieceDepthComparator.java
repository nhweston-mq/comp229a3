package au.edu.mq.students.s44882017.comp229.boardgame;

import java.util.Comparator;

class PieceDepthComparator implements Comparator<Piece> {

    @Override
    public int compare(Piece piece1, Piece piece2) {
        return Integer.compare(piece1.getDepth(), piece2.getDepth());
    }

}
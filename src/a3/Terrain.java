package au.edu.mq.students.s44882017.comp229.a3;

import au.edu.mq.students.s44882017.comp229.boardgame.CellType;

import java.awt.*;

public enum Terrain implements CellType {

    ROCK    (new Color(192, 192, 192)),
    DIRT    (new Color(128,  64,   0)),
    GRASS   (new Color(128, 255,   0)),
    TREES   (new Color(  0, 128,  64));

    private Color col;

    Terrain(Color col) {
        this.col = col;
    }

    @Override
    public Color getCol() {
        return col;
    }

    @Override
    public Image getImage() {
        return null;
    }

}

package au.edu.mq.students.s44882017.comp229.a3;

import au.edu.mq.students.s44882017.comp229.boardgame.celltypes.AssignByPriority;
import au.edu.mq.students.s44882017.comp229.boardgame.celltypes.DiamondSquare;

import java.awt.*;
import java.util.*;

public class TerrainGen extends AssignByPriority<Terrain> {

    private static final Terrain DEFAULT_TERRAIN = Terrain.ROCK;
    private static final double INCR_LIMIT_RATIO = 0.75;

    private static final Terrain[] TERRAINS = {
            Terrain.ROCK,
            Terrain.DIRT,
            Terrain.GRASS,
            Terrain.TREES
    };

    private static final double[] PROPORTIONS = {
            0.25,   // ROCK
            0.25,   // DIRT
            0.25,   // GRASS
            0.25    // TREES
    };

    public TerrainGen(int dimX, int dimY) {
        super(dimX, dimY, DEFAULT_TERRAIN);
        Map<Point, Double> scoresGrass, scoresDirt, scoresTrees;
        scoresGrass = new DiamondSquare(getDimX(), getDimY(), INCR_LIMIT_RATIO).get();
        scoresDirt  = new DiamondSquare(getDimX(), getDimY(), INCR_LIMIT_RATIO).get();
        scoresTrees = new Hashtable<>();
        for (int x=0; x<getDimX(); x++) for (int y=0; y<getDimY(); y++) {
            Point cell = new Point(x, y);
            scoresTrees.put(cell, scoresGrass.get(cell) + scoresDirt.get(cell));
        }
        setScoreTable(Terrain.GRASS,  scoresGrass);
        setScoreTable(Terrain.DIRT,   scoresDirt);
        setScoreTable(Terrain.TREES,  scoresTrees);
        for (int i=0; i<TERRAINS.length; i++) {
            setNumToAssign(TERRAINS[i], new Double(getDimX()*getDimY()*PROPORTIONS[i]).intValue());
        }
        assign(Collections.singleton(Terrain.TREES));
        assign(Arrays.asList(Terrain.GRASS, Terrain.DIRT));
        assign(Collections.singleton(Terrain.ROCK));
    }

}
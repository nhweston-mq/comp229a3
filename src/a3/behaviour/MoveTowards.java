package au.edu.mq.students.s44882017.comp229.a3.behaviour;

import au.edu.mq.students.s44882017.comp229.a3.piece.NPC;
import au.edu.mq.students.s44882017.comp229.boardgame.*;

import java.awt.*;
import java.time.Duration;
import java.time.Instant;

public class MoveTowards extends Behaviour {

    Board board;
    Class targetType;
    NPC piece;
    Piece target;
    Move moveNext;
    Instant timeOfNextMove;

    public MoveTowards(NPC piece, Class targetType) {
        this.board = piece.getBoard();
        this.piece = piece;
        this.targetType = targetType;
        target = board.findPieceByType(targetType);
        getNextMove();
    }

    @Override
    public void run() {
        try {
            while (piece.isActive()) {
                timeOfNextMove = piece.getTimeOfNextMove();
                long timeToNextMove = Duration.between(Instant.now(), timeOfNextMove).toMillis();
                if (timeToNextMove > 0) {
                    // Wait until allowed to make next move or target moves.
                    Thread.sleep(timeToNextMove);
                }
                if (!Instant.now().isBefore(timeOfNextMove)) {
                    getNextMove();
                    if (moveNext != null) {
                        moveNext.perform();
                    }
                }
            }
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void getNextMove() {
        if (!piece.isActive()) {
            return;
        }
        if (target == null || !target.isActive()) {
            target = board.findPieceByType(targetType);
            if (target == null || !target.isActive()) {
                return;
            }
        }
        Cell cellFrom = piece.getCell();
        Cell cellTarget = target.getCell();
        Cell cellTo;
        int dX = cellFrom.getDistX(cellTarget);
        int dY = cellFrom.getDistY(cellTarget);
        int dXAbs = Math.abs(dX);
        int dYAbs = Math.abs(dY);
        if (dX == 0 && dY == 0) {
            moveNext = null;
            return;
        }
        if (dXAbs >= dYAbs) {
            if (dX < 0) cellTo = board.getAdjacentCell(cellFrom, Direction.LEFT);
            else        cellTo = board.getAdjacentCell(cellFrom, Direction.RIGHT);
        }
        else {
            if (dY < 0) cellTo = board.getAdjacentCell(cellFrom, Direction.UP);
            else        cellTo = board.getAdjacentCell(cellFrom, Direction.DOWN);
        }
        moveNext = new Move(piece, cellFrom, cellTo);
    }

}

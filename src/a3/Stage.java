package au.edu.mq.students.s44882017.comp229.a3;

import au.edu.mq.students.s44882017.comp229.a3.behaviour.Behaviour;
import au.edu.mq.students.s44882017.comp229.a3.piece.*;
import au.edu.mq.students.s44882017.comp229.boardgame.Board;
import au.edu.mq.students.s44882017.comp229.boardgame.Piece;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.time.Instant;
import java.util.Set;

public class Stage extends Board<Terrain> implements KeyListener, MouseMotionListener {

    private static final int WIN_SIZE_X = 1280;
    private static final int WIN_SIZE_Y = 720;
    private static final int BOARD_POS_X = 10;
    private static final int BOARD_POS_Y = 10;
    private static final int BOARD_DIM_X = 20;
    private static final int BOARD_DIM_Y = 20;
    private static final int CELL_SIZE = 32;
    public static final int TURN_LENGTH = 2000;

    Player player;
    boolean isPlayerTurn;
    Instant timeOfTurnEnd;

    Stage() {
        super(BOARD_POS_X, BOARD_POS_Y, BOARD_DIM_X, BOARD_DIM_Y, CELL_SIZE);
        setPreferredSize(new Dimension(WIN_SIZE_X, WIN_SIZE_Y));
        reset();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        if (!isPlayerTurn && Instant.now().isAfter(timeOfTurnEnd)) {
            // NPC turn has ended.
            isPlayerTurn = true;
            player.setGfxVariant(0);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        char key = e.getKeyChar();
        boolean freeze = false;
        int dX = 0;
        int dY = 0;
        switch (key) {
            case 'w':   dY = -1;        break;
            case 's':   dY =  1;        break;
            case 'a':   dX = -1;        break;
            case 'd':   dX =  1;        break;
            case ' ':   freeze = true;  break;
            case 'r':   reset();        return;
            default:                    return;
        }
        if (isPlayerTurn && (freeze || player.processMove(dX, dY))) {
            isPlayerTurn = false;
            Instant timeOfTurnStart = Instant.now();
            timeOfTurnEnd = timeOfTurnStart.plusMillis(TURN_LENGTH);
            player.setGfxVariant(1);
            for (Piece piece : pieceSet()) {
                if (piece instanceof NPC) {
                    ((NPC)(piece)).nextTurn(timeOfTurnStart, TURN_LENGTH);
                }
            }
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {}

    @Override
    public void keyReleased(KeyEvent e) {}

    @Override
    public void mouseMoved(MouseEvent e) {
        Point mousePos = getMousePosition();
        if (mousePos != null) {
            // TODO
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {}

    private void reset() {
        generateTerrain(new TerrainGen(BOARD_DIM_X, BOARD_DIM_Y));
        player = new Player(this);
        isPlayerTurn = true;
        new Shepherd(this);
        new Sheep(this);
        new Wolf(this);
    }

}

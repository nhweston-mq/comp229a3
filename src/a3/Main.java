/*
 * COMP229 A3
 * Nicholas Weston (44882017)
 */

package au.edu.mq.students.s44882017.comp229.a3;

import au.edu.mq.students.s44882017.comp229.boardgame.GfxHandler;

import javax.swing.*;
import java.time.Duration;
import java.time.Instant;

public class Main extends JFrame implements Runnable {

    private static final int MILLIS_PER_FRAME = 20;
    private static final String PACKAGE_PREFIX = Main.class.getPackage().getName() + ".piece.";
    private static final String GFX_DIR = "gfx/";

    static {
        for (String[] gfxFilePath : new String[][] {
                {"Player",      "player.png",       "0" },
                {"Player",      "player_var.png",   "1" },
                {"Sheep",       "sheep.png",            },
                {"Shepherd",    "shepherd.png"          },
                {"Wolf",        "wolf.png"              }
        }) {
            String typeName = PACKAGE_PREFIX + gfxFilePath[0];
            String filePath = GFX_DIR + gfxFilePath[1];
            if (gfxFilePath.length > 2) {
                int variant = Integer.parseInt(gfxFilePath[2]);
                GfxHandler.set(typeName, filePath, variant);
            }
            else {
                GfxHandler.set(typeName, filePath);
            }
        }
    }

    public static void main(String[] args) {
        Main main = new Main();
        main.run();
    }

    private Stage stage;

    private Main() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        stage = new Stage();
        setContentPane(stage);
        addKeyListener(stage);
        addMouseMotionListener(stage);
        pack();
        setVisible(true);
    }

    public void run() {
        while (stage.isVisible()) {
            Instant timeInit = Instant.now();
            repaint();
            Instant timeFinal = Instant.now();
            try {
                Thread.sleep(MILLIS_PER_FRAME - Duration.between(timeInit, timeFinal).toMillis());
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}

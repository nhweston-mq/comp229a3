package au.edu.mq.students.s44882017.comp229.a3.piece;

import au.edu.mq.students.s44882017.comp229.a3.behaviour.MoveTowards;
import au.edu.mq.students.s44882017.comp229.boardgame.Board;
import au.edu.mq.students.s44882017.comp229.boardgame.Cell;
import au.edu.mq.students.s44882017.comp229.boardgame.Piece;

import java.awt.*;

public class Sheep extends NPC {

    public Sheep(Board board) {
        super(board);
        new Thread(new MoveTowards(this, Shepherd.class)).start();
    }

    @Override
    public int getMovement() {
        return 3;
    }

    @Override
    public void cellCohabited(Piece other) {
        if (other instanceof Wolf) {
            remove();
        }
        if (other instanceof Shepherd) {
            remove();
        }
    }

    @Override
    public void moveNotify(Cell cellFrom, Cell cellTo) {
        super.moveNotify(cellFrom, cellTo);
    }

}

package au.edu.mq.students.s44882017.comp229.a3.piece;

import au.edu.mq.students.s44882017.comp229.boardgame.Board;

import java.awt.*;

public class Shepherd extends NPC {

    public Shepherd(Board board) {
        super(board);
    }

    @Override
    public int getMovement() {
        return 1;
    }

}

package au.edu.mq.students.s44882017.comp229.a3.piece;

import au.edu.mq.students.s44882017.comp229.boardgame.*;

import java.awt.*;

public class Player extends Piece {

    private int gfxVariant;

    public Player(Board board) {
        super(board);
    }

    @Override
    public boolean isValidMove(Cell cellTo) {
        // Temporal restrictions in Stage.
        return cellTo.isAdjacentTo(getCell());
    }

    @Override
    public void paint(Graphics g, int posX, int posY) {
        g.drawImage(GfxHandler.get(getClass(), gfxVariant), posX, posY, null);
    }

    @Override
    public void cellCohabited(Piece other) {}

    public boolean processMove(int dX, int dY) {
        Move move = new Move(this, getCell(), getCell().getRelativeCell(dX, dY));
        return move.perform();
    }

    public void setGfxVariant(int gfxVariant) {
        System.out.println("Setting graphics variant to " + gfxVariant + ".");
        this.gfxVariant = gfxVariant;
    }

}

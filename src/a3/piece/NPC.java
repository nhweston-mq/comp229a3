package au.edu.mq.students.s44882017.comp229.a3.piece;

import au.edu.mq.students.s44882017.comp229.a3.behaviour.Behaviour;
import au.edu.mq.students.s44882017.comp229.boardgame.Board;
import au.edu.mq.students.s44882017.comp229.boardgame.Cell;
import au.edu.mq.students.s44882017.comp229.boardgame.GfxHandler;
import au.edu.mq.students.s44882017.comp229.boardgame.Piece;

import java.awt.*;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public abstract class NPC extends Piece {

    Queue<Instant> timesOfMoves;

    public NPC(Board board) {
        super(board);
    }

    public abstract int getMovement();

    @Override
    public void cellCohabited(Piece other) {}

    @Override
    public void paint(Graphics g, int posX, int posY) {
        g.drawImage(GfxHandler.get(getClass()), posX, posY, null);
    }

    @Override
    public boolean isValidMove(Cell cellTo) {
        if (timesOfMoves == null || timesOfMoves.isEmpty()) {
            return cellTo.isAdjacentTo(getCell());
        }
        if (Instant.now().isBefore(timesOfMoves.peek())) {
            timesOfMoves.poll();
        }
        return (
            (timesOfMoves == null || !Instant.now().isBefore(timesOfMoves.peek())) &&
            cellTo.isAdjacentTo(getCell())
        );
    }

    public synchronized void nextTurn(Instant timeOfMoveStart, long turnLength) {
        System.out.println(Thread.currentThread().getName() + " is running nextTurn().");
        int movement = getMovement();
        timesOfMoves = new LinkedList<>();
        if (movement > 0) for (int i=0; i<movement; i++) {
            timesOfMoves.add(timeOfMoveStart.plusMillis(i*turnLength/movement));
        }
        System.out.println(Thread.currentThread().getName() + " is notifying all.");
        notifyAll();
    }

    @Override
    public void moveNotify(Cell cellFrom, Cell cellTo) {
        timesOfMoves.poll();
    }

    public synchronized Instant getTimeOfNextMove() throws InterruptedException {
        if (timesOfMoves == null || timesOfMoves.isEmpty()) {
            System.out.println((Thread.currentThread().getName() + " is running getTimeOfNextMove()."));
            wait();
            System.out.println(Thread.currentThread().getName() + " has finished waiting.");
        }
        if (timesOfMoves.peek() == null) {
            System.out.println("WARNING: Time of move is still null!");
        }
        return timesOfMoves.peek();
    }

}
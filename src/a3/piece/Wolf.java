package au.edu.mq.students.s44882017.comp229.a3.piece;

import au.edu.mq.students.s44882017.comp229.a3.behaviour.MoveTowards;
import au.edu.mq.students.s44882017.comp229.boardgame.Board;

public class Wolf extends NPC {

    public Wolf(Board board) {
        super(board);
        new Thread(new MoveTowards(this, Sheep.class)).start();
    }

    @Override
    public int getMovement() {
        return 4;
    }

}
